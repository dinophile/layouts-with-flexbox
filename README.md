#WomenWhoCode Layouts-with-flexbox workshop

#Introduction/Purpose

This was a great workshop where we got to learn about and work with CSS Flexbox properties! So much fun and takes the 
sting out of positioning (in some cases! But so far I've used it wherever I could in my smaller exercises!)

##Deployment

This is a simple web project, deployment can be on any web server or local file system.
